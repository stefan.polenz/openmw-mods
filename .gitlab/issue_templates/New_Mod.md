- Name: (Name of the Mod to be added)
- Description: (Brief description of the mod, ideally from upstream)
- Homepage(s): (Location where the information about the mod can be found)
- Has this mod been tested to work with OpenMW?: (Can include personal testing, or sources such as modding-openmw.com or the openmw wiki)

## Source file locations

(List the download links, if possible. If it is from NexusMods, or another source where we cannot directly download files, note that, and list the file names)

## License Information

(List what permissions the author of the mod has provided, and the source of this information (readme, homepage, etc.))

(If there is an appropriate license included in the repository, list it)

(Note: We do not have the right to redistribute Bethesda's original morrowind assets, or derivatives of them. If a mod contains original morrowind assets, or direct modifications of them, we should consider it to be all-rights-reserved)

## Dependencies

(List any mods that are required by this mod to function correctly, as well as any known conflicts with other mods, even if the other mods are not yet present in the repository)

## Optional Features and Archive Structure

(List any optional features which are included with this mod. It may also be useful to note which files and directories need to be installed for each optional feature)

/label new-mod
