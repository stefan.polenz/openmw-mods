# a Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Morrowind"
    DESC = "Base morrowind data files by Bethesda"
    HOMEPAGE = "https://elderscrolls.bethesda.net/en/morrowind"
    KEYWORDS = "openmw"
    IUSE = "bloodmoon tribunal"
    TIER = "0"
    # Needs the morrowind_path.txt file to exist, so morrowind-data must be installed first
    DEPEND = ">=common/mw-2 base/morrowind-data[bloodmoon=,tribunal=]"
    # Technically there is a specific EULA, however the user would
    # have accepted it already when installing the files.
    LICENSE = "all-rights-reserved"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("Morrowind.esm"),
                File(
                    "Bloodmoon.esm", REQUIRED_USE="bloodmoon", OVERRIDES="Tribunal.esm"
                ),
                File("Tribunal.esm", REQUIRED_USE="tribunal"),
            ],
        )
    ]

    def src_install(self):
        with open(os.path.join(self.ROOT, "etc", "morrowind_path.txt")) as file:
            path = file.read().strip()

        metadata = self.get_metadata()
        metadata["path"] = path
        metadata.update(self.INSTALL_DIRS[0].dump(self))
        self.write_metadata(metadata)
